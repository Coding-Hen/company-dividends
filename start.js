let _modPath;

exports.initialize = modPath => {
	_modPath = modPath;
};

exports.onLoadGame = settings => {
	$rootScope = GetRootScope();
	if($rootScope.options.language === "de"){
		Language["dividend_transaction"] = "Dividendenzahlung";
		Language["dividends_payout"] = "Wöchentliche Dividenden generiert: $";
	}else if($rootScope.options.language === "es"){
		Language["dividend_transaction"] = "Pago de dividendos";
		Language["dividends_payout"] = "Dividendos semanales generados: $";
	}else if($rootScope.options.language === "fr"){
		Language["dividend_transaction"] = "Paiement de dividendes";
		Language["dividends_payout"] = "Dividendes hebdomadaires générés: $";
	}else if($rootScope.options.language === "br"){
		Language["dividend_transaction"] = "Pagamento de dividendos";
		Language["dividends_payout"] = "Dividendos semanais gerados: $";
	}else if($rootScope.options.language === "cn"){
		Language["dividend_transaction"] = "Gǔxí zhīfù";
		Language["dividends_payout"] = "Měi zhōu chǎnshēng gǔxí: $";
	}else if($rootScope.options.language === "ru"){
		Language["dividend_transaction"] = "Выплата дивидендов";
		Language["dividends_payout"] = "Недельные дивиденды: $";
	}else if($rootScope.options.language === "tr"){
		Language["dividend_transaction"] = "Temettü Ödeme";
		Language["dividends_payout"] = "Haftalık temettü oluşumu: $";
	}else if($rootScope.options.language === "it"){
		Language["dividend_transaction"] = "Pagamento dei dividendi";
		Language["dividends_payout"] = "Dividendi settimanali generati: $";
	}else if($rootScope.options.language === "nl"){
		Language["dividend_transaction"] = "Uitbetaling van dividend";
		Language["dividends_payout"] = "Wekelijkse dividenden gegenereerd: $";
	}else if($rootScope.options.language === "hu"){
		Language["dividend_transaction"] = "Osztalékfizetés";
		Language["dividends_payout"] = "Heti osztalék keletkezik: $";
	}else if($rootScope.options.language === "kr"){
		Language["dividend_transaction"] = "Baedang-geum";
		Language["dividends_payout"] = "Saengseong doen jugan baedang-geum: $";
	}else{
		Language["dividend_transaction"] = "Dividend Payout";
		Language["dividends_payout"] = "Weekly dividends generated: $";
	}
	Game.Lifecycle._onNewWeek = (()=>{
		Helpers.UpdateCompetitors();
		const competitorProducts = GetRootScope().settings.competitorProducts;
		var totalPayout = 0;
		competitorProducts.forEach(function(competitor)
		{
			if(competitor.stockTransactions.length != 0 && !competitor.merged && competitor.history[competitor.history.length-1].stockPrice > competitor.history[competitor.history.length-8].stockPrice){
				var ownedStocks = _.sum(competitor.stockTransactions.map(x => x.amount));
				var payout = competitor.history[competitor.history.length-1].stockPrice / 100 * (Math.random() * (5 - 2) + 2) * ownedStocks;
				if (payout > 0){
					totalPayout += payout;
					Game.Lifecycle.$rootScope.settings.balance += payout;
					Game.Lifecycle.$rootScope.addTransaction(Helpers.GetLocalized("dividend_transaction") + ": " + competitor.name, payout);
				}
			}
		});
		if(totalPayout != 0){
			Helpers.ShowNotification(Helpers.GetLocalized("dividends_payout") + numeral(totalPayout).format("0,0"));
		}
	});
	
};